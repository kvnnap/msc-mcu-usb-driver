This is the README file for the msc-mcu-usb-driver code.

The msc-mcu-usb-driver contains code that implements the USB protocol and that
drives the LCD controller. Therefore, it acts as an intermediate device between 
the Linux host and the LCD controller.

These programs are required prior to compilation:

1) avr-gcc
2) avrdude
3) make 

If not available, run 

> sudo apt-get install gcc-avr binutils-avr avr-libc make

To compile the MCU code execute make:

> make

The program is now compiled and the main.elf/main.hex files should now be available.

To flash the MCU with the compiled program, execute:

> sudo make flashhex

or 

> sudo make flashelf

both will work.

On success, the code is flashed onto the MCU and starts right away.
