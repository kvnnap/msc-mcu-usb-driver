# Using g++ compiler
CC = avr-gcc
AVRDUDE = avrdude
MCU = atmega328p

# Standard
STANDARD =

# Include paths
INCLUDE_PATH = -I./include -I./include/usbdrv
LIBRARY_PATH =

# Optimisation
OPTIMISATION = -Os

# Name of the output
NAME = main
HEX = $(NAME).hex
EXECUTABLE = $(NAME).elf

# Use bits example -m64
ARCHITECTURE = -mmcu=$(MCU)

# Parameters passed in linking
# -Wl,-u,vfprintf -lprintf_flt -lm = add if printing float variables
LFLAGS = $(LIBRARY_PATH) $(ARCHITECTURE)

# Parameters passed in compilation
CFLAGS = -Wall $(STANDARD) $(INCLUDE_PATH) $(ARCHITECTURE) $(OPTIMISATION) -DF_CPU=16000000

# Useful functions
rwildcard = $(wildcard $(1)$(2)) $(foreach d,$(wildcard $(1)*),$(call rwildcard,$(d)/,$(2)))

# Sependent object files - expand immediately
INCLUDE := $(call rwildcard,./include/,*.h)
SOURCES := $(call rwildcard,./src/,*.c) $(call rwildcard,./src/,*.S)
OBJECTS := $(patsubst ./src/%.c,./obj/%.o,$(SOURCES))
OBJECTS := $(patsubst ./src/%.S,./obj/%.o,$(OBJECTS))
DIRECTORIES := ./obj/ $(patsubst ./src/%,./obj/%,$(filter-out $(call rwildcard,./src/,*.*), $(call rwildcard,./src/,*)))

all hex: $(HEX)

elf: $(EXECUTABLE)

flashhex: $(HEX)
	$(AVRDUDE) -c usbasp -p $(MCU) -U flash:w:$<:i

flashelf: $(EXECUTABLE)
	$(AVRDUDE) -c usbasp -p $(MCU) -U flash:w:$<:e

debug: OPTIMISATION =
debug: CFLAGS := -g $(CFLAGS)
debug: LFLAGS := -g $(LFLAGS)
debug: $(HEX)

# The hex depends on the executable
$(HEX): $(EXECUTABLE)
	avr-objcopy -O ihex $< $@
	avr-size $@

# The executable depends on the objects.. Link Them!
$(EXECUTABLE): $(DIRECTORIES) $(OBJECTS)
	$(CC) $(OBJECTS) $(LFLAGS) -o $@
	avr-size $@

# Every something.o depends on something.c
# $< is the name of the first dependency
# All source files depend on all headers
./obj/%.o: ./src/%.c $(INCLUDE)
	$(CC) $(CFLAGS) -c $< -o $@

./obj/%.o: ./src/%.S $(INCLUDE)
	$(CC) $(CFLAGS) -c $< -o $@

# obj directory creation
./obj/ ./obj/%:
	mkdir $@

clean:
	rm -rf obj $(EXECUTABLE) $(HEX)
