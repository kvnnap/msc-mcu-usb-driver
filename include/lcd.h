#ifndef LCD
#define LCD

#include <avr/io.h>
#include <inttypes.h>
typedef enum { false, true } bool;

/**** Define which Port, Direction and Pin to use. 
      Obviously use the same letter for all *************************************/
#define LCD_PIN  PINC
#define LCD_DDR  DDRC
#define LCD_PORT PORTC

#define LCD_DDRE DDRD
#define LCD_PORTE PORTD
/********************************************************************************/

/**** Wiring map - This will be a 4 bit setup - hence we need 7 pins in total ***/
//Data Pins - Can be set as Input or Output
#define LCD_DB4 0x01 //Attach the LCD DB4    Pin to ATMega Port bit 0 (0-based)
#define LCD_DB5 0x02 //Attach the LCD DB5    Pin to ATMega Port bit 1 (0-based)
#define LCD_DB6 0x04 //Attach the LCD DB6    Pin to ATMega Port bit 2 (0-based)
#define LCD_DB7 0x08 //Attach the LCD DB7    Pin to ATMega Port bit 3 (0-based)

//Control Pins - Set as Output Only
#define LCD_CRS 0x10 //Attach the LCD RS     Pin to ATMega Port bit 4 (0-based)
#define LCD_CRW 0x20 //Attach the LCD RW     Pin to ATMega Port bit 5 (0-based)

// This can be defined a separate port, check LCD_PORTE and others, example:
// Just set LCD_DDRE and LCD_PORTE to LCD_DDR and LCD_PORT and set LCD_CE to 0x40
#define LCD_CE  0x01 //Attach the LCD Enable Pin to ATMega Port bit 0 (0-based)
/********************************************************************************/

/***************************** Useful Constants *********************************/
//LCD_CONTROL = 0x30, MASK_LCD_CONTROL = 0xCF,
//LCD_DATA    = 0x0F, MASK_LDC_DATA    = 0xF0
#define LCD_CONTROL      (/*LCD_CE | */LCD_CRW | LCD_CRS)
#define MASK_LCD_CONTROL (~LCD_CONTROL)
#define LCD_DATA         (LCD_DB7 | LCD_DB6 | LCD_DB5 | LCD_DB4)
#define MASK_LCD_DATA    (~LCD_DATA)
/********************************************************************************/

/****************** Bus Timing Characteristics in Nanoseconds *******************/
//Low Voltage              is defined from 2.7 to 4.5 volts
//Normal Operating Voltage is defined from 4.5 to 5.5 volts
//#define LCD_LOW_VOLTAGE

#ifdef LCD_LOW_VOLTAGE

//Common for read and write
#define LCD_ENABLE_CYCLE_TIME 1000
#define LCD_ENABLE_PULSE_WIDTH_HIGH_TIME 450
#define LCD_MAX_ENABLE_RISE_FALL_TIME 25
#define LCD_ADDRESS_SETUP_TIME 60
#define LCD_ADDRESS_HOLD_TIME 20
//For Write
#define LCD_DATA_SETUP_TIME 195
#define LCD_WRITE_DATA_HOLD_TIME 10
//For Read
#define LCD_MAX_DATA_DELAY_TIME 360
#define LCD_READ_DATA_HOLD_TIME 5

#else

//Common for read and write
#define LCD_ENABLE_CYCLE_TIME 500
#define LCD_ENABLE_PULSE_WIDTH_HIGH_TIME 230
#define LCD_MAX_ENABLE_RISE_FALL_TIME 20
#define LCD_ADDRESS_SETUP_TIME 40
#define LCD_ADDRESS_HOLD_TIME 10
//For Write
#define LCD_DATA_SETUP_TIME 80
#define LCD_WRITE_DATA_HOLD_TIME 10
//For Read
#define LCD_MAX_DATA_DELAY_TIME 160
#define LCD_READ_DATA_HOLD_TIME 5

#endif
/********************************************************************************/

void lcd_init (const bool twoDisplayLines, const bool largeFont);
uint8_t lcd_read_IR_until_not_busy();
uint8_t lcd_read(const bool fromDataRegister);
uint8_t lcd_get_address();
uint8_t lcd_write(const bool toDataRegister, const uint8_t data);

/* Only use below functions preferably */

uint8_t lcd_clear_display();
uint8_t lcd_return_home();
uint8_t lcd_set_entry_mode(const bool increment, const bool shift);
uint8_t lcd_set_display_control(const bool displayOn, const bool cursorOn, const bool blinkingOn);
uint8_t lcd_set_cursor_display_shift(const bool displayShift, const bool right);
uint8_t lcd_set_CGRAM_address (const uint8_t cgramAddress);
uint8_t lcd_set_DDRAM_address (const uint8_t ddramAddress);
uint8_t lcd_write_data(const uint8_t data);
uint8_t lcd_read_data();
void lcd_write_string(const char * string);
void lcd_writeln_string(const char * string);
void lcd_write_string_len(const char * string, uint8_t len);
void lcd_read_string(char * buff, uint8_t len);
#endif
