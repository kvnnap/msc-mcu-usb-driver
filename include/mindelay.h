#ifndef MIN_DELAY
#define MIN_DELAY

#include <avr/io.h>
#include <inttypes.h>

// #define F_CPU 16000000UL
#define __DELAY_BACKWARD_COMPATIBLE__

#define F_CPU_GHZ (((float)F_CPU) / 1000000000UL)
#define F_CPU_MHZ (F_CPU / 1000000UL)
#define F_CPU_KHZ (F_CPU / 1000UL)

#define T_CPU_NS (1000000000UL / (float)F_CPU)
#define T_CPU_US (1000000UL / (float)F_CPU)
#define T_CPU_MS (1000UL / (float)F_CPU)


void waitNanoSeconds(uint16_t ns);
void waitMicroSeconds(uint16_t ms);
void waitMilliSeconds(uint16_t ms);

void resetMicro();
float micro();

void resetMicroBroad();
float microBroad();

void setup_fpwm_2();
void set_duty_on(uint8_t n);

#endif
