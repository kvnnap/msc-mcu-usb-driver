#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>  /* for sei() */
#include <util/delay.h>     /* for _delay_ms() */

#include <avr/pgmspace.h>   /* required by usbdrv.h */
#include "usbdrv.h"
#include "oddebug.h"        /* This is also an example for using debug macros */
#include "lcd.h"
#include <stdio.h>

static char buffer[32]; // no need to assign as global variables are initialised to zero
static uchar currentPosition, bytesRemaining, offset;
static usbMsgLen_t len;

// This function changes offset and bytesRemaining
// and data of buffer, uses len to determine amount but will not change it
static void readOrWriteLCD(void f(char *, uint8_t)) {
    // Read or Write data directly from/to LCD Device
    bytesRemaining = len;
    if (offset < 16) {
    	lcd_set_DDRAM_address(0x00 + offset);
    	uint8_t numToReadWrite = len <= 16 - offset ? len : 16 - offset;
    	f(buffer + offset, numToReadWrite);
    	bytesRemaining = len - numToReadWrite;
    	offset = 16;
    }

    if (bytesRemaining > 0) {
    	lcd_set_DDRAM_address(0x40 + offset - 16);
    	f(buffer + offset, bytesRemaining);
    }
}

usbMsgLen_t usbFunctionSetup(uchar data[8])
{
    usbRequest_t * rq = (void *)data;

    offset = (uchar)rq->wIndex.word;

    len = rq->wLength.word > sizeof(buffer) - offset ? 
    		sizeof(buffer) - offset : 
    		rq->wLength.word;

    if (offset >= sizeof(buffer) || len == 0) {
    	return 0;
    }

    if ((rq->bmRequestType & USBRQ_DIR_MASK) == USBRQ_DIR_HOST_TO_DEVICE) {
    	// OUT Endpoint - Host to Device
    	// We can receive data from host here using usbFunctionWrite..

        PORTB ^= 0x04;

	bytesRemaining = len;		// store the amount of data requested
        currentPosition = offset;	// initialize position index
        
        return USB_NO_MSG;        // tell driver to use usbFunctionWrite()

    } else {
    	// IN Endpoint - Device to Host
    	// We can send back to device either here or using usbFunctionRead..

        PORTB ^= 0x02;

        readOrWriteLCD(lcd_read_string);

    	usbMsgPtr = (usbMsgPtr_t)buffer;
    	
    	return len;
    }

    return 0;   /* default for not implemented requests: return no data back to host */
}

uchar usbFunctionWrite(uchar *data, uchar prtLen)
{
    uchar i;

    if(bytesRemaining < prtLen)                // if this is the last incomplete chunk
        prtLen = bytesRemaining;               // limit to the amount we can store
    bytesRemaining -= prtLen;
    for(i = 0; i < prtLen; ++i) {
        buffer[currentPosition++] = data[i];
    }

    if (bytesRemaining == 0) {
        // delays here are risky but somewhat recoverable

        // Write data directly to LCD Device
        readOrWriteLCD((void(*)(char *, uint8_t))lcd_write_string_len);

        return 1;
    }

    return 0;             // return 1 if we have all data
}

/* ------------------------------------------------------------------------- */

int __attribute__((noreturn)) main(void)
{
    lcd_init(true, false);
    lcd_clear_display();
    //lcd_write_string(dataBuffer);

    //wdt_enable(WDTO_1S);
    /* Even if you don't use the watchdog, turn it off here. On newer devices,
     * the status of the watchdog (on/off, period) is PRESERVED OVER RESET!
     */
    /* RESET status: all port bits are inputs without pull-up.
     * That's the way we need D+ and D-. Therefore we don't need any
     * additional hardware initialization.
     */
    usbInit();
    usbDeviceDisconnect();  /* enforce re-enumeration, do this while interrupts are disabled! */
    uchar i = 0;
    while(--i){             /* fake USB disconnect for > 250 ms */
        //wdt_reset();
        _delay_ms(1);
    }
    usbDeviceConnect();

    sei();
    DDRB |= 0x06;
    for(;;){                /* main event loop */
        //wdt_reset();
        usbPoll();
    }
}

/* ------------------------------------------------------------------------- */
