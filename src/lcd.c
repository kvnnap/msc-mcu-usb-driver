#include "mindelay.h"
#include "lcd.h"

/**
  * After execution of the CGRAM/DDRAM data write or read instruction, the RAM address counter
  * is incremented or decremented by 1. The RAM address counter is updated after the busy flag
  * turns off. In Figure 10, tADD is the time elapsed after the busy flag turns off until the address
  * counter is updated.
**/
static bool waitForAddressCounterWhileReading = false;

/* Spins until busy flag is not set. 
 * Returns contents of IR while at it.
 */
uint8_t lcd_read_IR_until_not_busy() {
	uint8_t readData;
	while ((readData = lcd_read(false)) & 0x80) {
		// you spin me round, baby, right round like a record, baby.
	}
	return readData;
}

/**
  * Write Mode:  
  *    Will transfer a byte of data to either the Data Register or the Instruction Register.
  *    This means it will do two 4-bit transfers.
  */
void lcd_write_cycles(const bool toDataRegister, const uint8_t data, const bool sendHighNibbleOnly) {
	// Assuming LCD_CE is OFF before this method is called

	//Change Direction of Data Pins to be OUTPUT
	LCD_DDR |= LCD_DATA;

	const uint8_t cycles = sendHighNibbleOnly ? 1 : 2;
	uint8_t cycle;
	for (cycle = 0; cycle < cycles; ++cycle) {

		// MASK ALL PINS - hence we will get zero's in all our pin locations and
		// Set RS and Unset RW and Set Data ( We can set data now because it is irrelavant when we do it as long 
		// as we set it LCD_DATA_SETUP_TIME before we Turn Off Enable)
		LCD_PORT = (LCD_PORT & ~(LCD_CONTROL | LCD_DATA)) | (toDataRegister ? LCD_CRS : 0) | ((data >> (cycle == 0 ? 4 : 0)) & LCD_DATA);

		//Wait LCD_ADDRESS_SETUP_TIME nano seconds before Enabling
		waitNanoSeconds(LCD_ADDRESS_SETUP_TIME);

		//Now Enable
		LCD_PORTE |= LCD_CE;

		//no minimum time to wait, but max time for Enable Rise is defined.
		//we cannot do anything here other than hope that LCD_CE rises to
		//Vcc in less than LCD_MAX_ENABLE_RISE_FALL_TIME time (20ns to 25ns).

		//Since data is already setup, we will wait for the LCD_ENABLE_PULSE_WIDTH_HIGH_TIME time.
		//LCD_DATA_SETUP_TIME time is included in this, we will include max Rise Time just in case.
		waitNanoSeconds(LCD_ENABLE_PULSE_WIDTH_HIGH_TIME + LCD_MAX_ENABLE_RISE_FALL_TIME);

		// Now we Turn Off Enable bit
		LCD_PORTE &= ~LCD_CE;

		//no minimum time to wait, but max time for Enable Fall is defined.
		//we cannot do anything here other than hope that LCD_CE falls to
		//GND in less than LCD_MAX_ENABLE_RISE_FALL_TIME time (20ns to 25ns).

		//Now we hold the data on the data bus for LCD_WRITE_DATA_HOLD_TIME
		//plus LCD_MAX_ENABLE_RISE_FALL_TIME just in case.
		//waitNanoSeconds(LCD_WRITE_DATA_HOLD_TIME + LCD_MAX_ENABLE_RISE_FALL_TIME);

		//transfer should now be complete, but we need to wait for LCD_ENABLE_CYCLE_TIME
		//in total (after ADDRESS Setup Time has passed) to complete this LCD Cycle
		//waitNanoSeconds(LCD_ENABLE_CYCLE_TIME - 2 * LCD_MAX_ENABLE_RISE_FALL_TIME - LCD_ENABLE_PULSE_WIDTH_HIGH_TIME - LCD_WRITE_DATA_HOLD_TIME);

		//joining the two waits we get the remaining total wait of
		waitNanoSeconds(LCD_ENABLE_CYCLE_TIME - LCD_MAX_ENABLE_RISE_FALL_TIME - LCD_ENABLE_PULSE_WIDTH_HIGH_TIME);	
		 
		//cycle is now ready
	}

	// Transfer is now ready

	// Power Saving on Data Pins, Setting them as High-Z Tristate
	LCD_DDR &= MASK_LCD_DATA;
	LCD_PORT &= MASK_LCD_DATA;
}

//waits for device to be ready, then writes the data
//returns the data of the Instruction Register just BEFORE the WRITE Operation occurs
uint8_t lcd_write(const bool toDataRegister, const uint8_t data) {

	//let's wait for device to be ready before writing, shall we?
	//we do that by reading busy flag
	uint8_t readData = lcd_read_IR_until_not_busy();

	lcd_write_cycles(toDataRegister, data, false);

	//Handle strange documentation case in Figure 10
	waitForAddressCounterWhileReading = toDataRegister;

	return readData;
}

/** Don't use this fucntion to read ADDRESS from Instruction Register! 
    However if only interested in Busy flag this should be fine
    **/
uint8_t lcd_read_cycles(const bool fromDataRegister) {

	// Assuming LCD_CE is OFF before this method is called

	//Change Direction of Data Pins to be Input
	LCD_DDR &= MASK_LCD_DATA;

	// MASK ALL PINS - hence we will get zero's in all our pin locations and
	// Set RW (Read) and Set RS and UnSet Data Pull Up Resistors (it consumes power, set tristate) 
	LCD_PORT = (LCD_PORT & ~(LCD_CONTROL | LCD_DATA)) | LCD_CRW | (fromDataRegister ? LCD_CRS : 0)/* | LCD_DATA*/;

	uint8_t data = 0;
	uint8_t cycle;

	for (cycle = 0; cycle < 2; ++cycle) {
		
		//Wait LCD_ADDRESS_SETUP_TIME nano seconds before Enabling
		waitNanoSeconds(LCD_ADDRESS_SETUP_TIME);

		//Now Enable
		LCD_PORTE |= LCD_CE;

		//no minimum time to wait, but max time for Enable Rise is defined.
		//we cannot do anything here other than hope that LCD_CE rises to
		//Vcc in less than LCD_MAX_ENABLE_RISE_FALL_TIME time (20ns to 25ns).

		//We need to wait for LCD_MAX_ENABLE_RISE_FALL_TIME + LCD_MAX_DATA_DELAY_TIME
		//before data on the data bus becomes valid
		waitNanoSeconds(LCD_MAX_DATA_DELAY_TIME + LCD_MAX_ENABLE_RISE_FALL_TIME);

		// data is now valid, we can read it
		data <<= 4;
		data |= LCD_PIN & LCD_DATA;

		//We need to wait the rest of the LCD_ENABLE_PULSE_WIDTH_HIGH_TIME time.
		waitNanoSeconds(LCD_ENABLE_PULSE_WIDTH_HIGH_TIME - LCD_MAX_DATA_DELAY_TIME);

		// Now we Turn Off Enable bit
		LCD_PORTE &= ~LCD_CE;

		//no minimum time to wait, but max time for Enable Fall is defined.
		//we cannot do anything here other than hope that LCD_CE falls to
		//GND in less than LCD_MAX_ENABLE_RISE_FALL_TIME time (20ns to 25ns).

		//Now we wait for LCD_MAX_ENABLE_RISE_FALL_TIME fall time plus the rest of LCD_ENABLE_CYCLE_TIME
		waitNanoSeconds(LCD_ENABLE_CYCLE_TIME - LCD_MAX_ENABLE_RISE_FALL_TIME - LCD_ENABLE_PULSE_WIDTH_HIGH_TIME);	
		 
		//cycle is now ready
	}

	// Transfer is now ready
	return data;
}

uint8_t lcd_read(const bool fromDataRegister) {
	//only reads to the instruction register can be executed while device is busy
	//hence if we're reading from data register we must wait till device is ready..
	if (fromDataRegister) {
		lcd_read_IR_until_not_busy();
	}

	uint8_t read = lcd_read_cycles(fromDataRegister);

	//Handle strange documentation case in Figure 10
	waitForAddressCounterWhileReading = fromDataRegister;

	return read;
}


/**
  * Use this method to make sure that we get a valid Address from the insturction register.
  * Since according to documentation, there is delay for data to become valid after
  * busy flag is set.
  * If no data reads or writes have occured before, this method returns immediately.
  * RS = 0 (Instruction Register), R/!W = 1 (Read)
  * Returns just the address without the busyflag - busyflag should be zero anyway
  */
uint8_t lcd_get_address() {
	//recurse due to invalid address - if waitForAddressCounterWhileReading && interested in Address
	if (waitForAddressCounterWhileReading) {
		//waitForAddressCounterWhileReading = false; - set inside lcd_read

		// angry spin cos this design was a stupid design.. 
		// Why would you unset the busy flag before the address data becomes valid??
		// Fuck you Hitachi
		lcd_read_IR_until_not_busy();
		//wait 4us, better wait for 8us to be sure
		waitMicroSeconds(8);
	}

	return lcd_read(false) & 0x7F; //No Anding required, but just in case..
}

// Initialise device by instruction in 4-bit mode and small font and 2 lines
void lcd_init (const bool twoDisplayLines, const bool largeFont) {

	waitForAddressCounterWhileReading = false;

	// Setup Enable Pin for Output and put it to zero
	LCD_DDRE |= LCD_CE;
	LCD_PORTE &= ~LCD_CE;

	//Setup All LED Pins for Output and put them to Zero
	LCD_DDR |= LCD_CONTROL | LCD_DATA;
	LCD_PORT &= ~(LCD_CONTROL | LCD_DATA);

	//start 4-bit initialization procedure

	//wait for 100ms to be safe just in case voltage rises slowly (40ms is written in doc)
	waitMilliSeconds(100);
	//send first reset pulse - we're in 8-bit mode still so send largest nibble
	lcd_write_cycles(false, 0x30, true);
	//wait for 5ms to be safe (4.1ms is written in doc)
	waitMilliSeconds(5);
	//send second reset pulse - we're in 8-bit mode still so send largest nibble
	lcd_write_cycles(false, 0x30, true);
	//wait for 100 microseconds
	waitMicroSeconds(100);
	//send third and last reset pulse - we're in 8-bit mode still so send largest nibble
	lcd_write_cycles(false, 0x30, true);

	//not known if we can use busy flag here, TEST LATER, in the mean time, wait for 100us
	waitMicroSeconds(100);
	//Inital Function Set starts here - Convert to 4-bit mode
	lcd_write_cycles(false, 0x20, true);

	
	//not known if we can use busy flag here, TEST LATER, in the mean time, wait for 100us
	waitMicroSeconds(100);
	//Since we just changed to 4-bit mode, we're allowed to send another Function Set command
	//so that we can provide the full parameters to it by transfering all 8 bytes in two writes
	lcd_write_cycles(false, 0x20 | (twoDisplayLines ? 0x08 : 0) | (largeFont ? 0x04 : 0), false); //keep 4-bit flag, N = 1 means two lines, and Font is a don't care here

	//now we can use busy flag, thus we can use the normal write procedure
	//* write 0x08 - Display OFF
	lcd_write(false, 0x08);
	
	//* write 0x01 - Display Clear
	lcd_write(false, 0x01);
	//* write 0x06 - Entry mode set, I/D = 1 (Increment by 1), S = 0 (No Shift)
	lcd_write(false, 0x06);

	//device initialization ready - Turn on display and clear RAM Values to blanks (0x20)
	lcd_write(false, 0x0F); // turn on Display with cursor and blinking
	lcd_write(false, 0x01); // Clear display - return home plus clear DDRAM setting all positions to 0x20 (space)

	//done
}

/** High Level Functions **/

/**
Clears DDRAM by writing Spaces 0x20 into all the DDRAM
Sets DDRAM address to 0 and returns display to original status if it was shifted
also cursor and blinking go to starting position (same as in lcd_return_home)
Sets Increment Mode to 1, meaning to the right, thus reading or writing to Ram
will increase the address counter by 1
**/
uint8_t lcd_clear_display() {
	return lcd_write(false, 0x01);
}

/**
Sets DDRAM address to 0 and returns display to original status if it was shifted
also cursor and blinking go to starting position (supposed to take 1.52ms)
**/
uint8_t lcd_return_home() {
	return lcd_write(false, 0x02);
}

/**
Sets whether the address should increment or decrement when data is read or
written to Ram. Also, sets whether the display should start shifting. If display 
shifts, then cursors and blinking will not move but entire display will (only when WRITING)
**/
uint8_t lcd_set_entry_mode(const bool increment, const bool shift) {
	return lcd_write(false, 0x04 | (increment ? 0x02 : 0) | (shift ? 0x01 : 0));
}

/**
Sets various display settings. Parameters are self explenatory.
**/
uint8_t lcd_set_display_control(const bool displayOn, const bool cursorOn, const bool blinkingOn) {
	return lcd_write(false, 0x08 | (displayOn ? 0x04 : 0) | (cursorOn ? 0x02 : 0) | (blinkingOn ? 0x01 : 0));
}

/**
Cursor or display shift shifts the cursor position or display to the right or left without writing or reading
display data (Table 7). Note that the first and second line displays will shift at the same time.
The address counter (AC) contents will not change if the only action performed is a DISPLAY shift.
**/
uint8_t lcd_set_cursor_display_shift(const bool displayShift, const bool right) {
	return lcd_write(false, 0x10 | (displayShift ? 0x08 : 0) | (right ? 0x04 : 0));
}

/** Function set should not be used, therefore it will not be implement 
	Call lcd_init() if needed.. but need to implement parameters here.
**/

/** Set the address of CGRAM to be read or written to 
  * by the write/read data insturctions.
  * Custom patterns can be sent here.
  */
uint8_t lcd_set_CGRAM_address (const uint8_t cgramAddress) {
	return lcd_write(false, 0x40 | (cgramAddress & 0x3F) );
}

/** Set the address of DDRAM to be read or written to 
  * by the write/read data insturctions.
  * Locations in CGROM memory and CGRAM can then be stored 
    to be displayed.
    Address range is from 0x00 to 0x27 and 0x40 to 0x67
  */
uint8_t lcd_set_DDRAM_address (const uint8_t ddramAddress) {
	return lcd_write(false, 0x80 | (ddramAddress & 0x7F) );
}


/** Writes data to the set Address Ram
**/
uint8_t lcd_write_data(const uint8_t data) {
	return lcd_write(true, data);
}

/** Reads data to the set Address Ram - CANNOT Understand this on page 31 yet..
    Should I set Address each time? Is this related to Waiting of address (page 25) which 
    was already catered for??
    Doc says: 
The address counter (AC) is automatically incremented or decremented by 1 after the write
instructions to CGRAM or DDRAM are executed. The RAM data selected by the AC cannot be
read out at this time even if read instructions are executed. Therefore, to correctly read data,
execute either the address set instruction or cursor shift instruction (only with DDRAM), then just
before reading the desired data, execute the read instruction from the second time the read
instruction is sent
**/
uint8_t lcd_read_data() {
	return lcd_read(true);
}

/* must be null terminated - will write at current pos */
void lcd_write_string(const char * string) {
	char c;
	while ( (c = *(string++) ) ) {
		lcd_write_data((uint8_t)c);
	}
}

void lcd_write_string_len(const char * string, uint8_t len) {
	uint8_t i;
	for (i = 0; i < len; ++i) {
		lcd_write_data((uint8_t)string[i]);
	}
}

/* Copies all data from second line to first line and write to second line */
void lcd_writeln_string(const char * string) {
	
	char prevString[17];
	uint8_t count;
	lcd_set_DDRAM_address(0x40);
	for (count = 0; count < 16; ++count) {
		prevString[count] = (char)lcd_read_data();
	}
	prevString[16] = 0;
	//clear screen
	lcd_clear_display();
	//write data to first line
	lcd_write_string(prevString);
	lcd_set_DDRAM_address(0x40);
	lcd_write_string(string);
}

/* Reads up to n bytes of data from the current address (DDRAM ) */
void lcd_read_string(char * buff, uint8_t len) {
	while (len--) {
		*(buff++) = (uint8_t) lcd_read_data();
	}
}

/* Works but much slower!
void lcd_writeln_string(const char * string) {
	
	uint8_t address = 0;

	for(address = 0; address < 16; ++address) {
		lcd_set_DDRAM_address(0x40 + address);
		uint8_t datum = lcd_read_data();
		//after read, address increments so set it again
		lcd_set_DDRAM_address(0x40 + address);
		lcd_write_data(0x20); //blank it out
		//write to first line
		lcd_set_DDRAM_address(0x00 + address);
		lcd_write_data(datum);
	}

	lcd_set_DDRAM_address(0x40);
	lcd_write_string(string);
}*/

